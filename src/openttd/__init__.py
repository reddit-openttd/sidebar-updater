# OpenTTD python module
# http://openttd-python.googlecode.com/

# Ensure the user is running the version of python we require.
import sys
if not hasattr(sys, "version_info") or sys.version_info < (3,8):
    raise RuntimeError("OpenTTD-Python requires Python 3.8 or later.")
del sys

# from datastorageclass import DataStorageClass
# from packet import DataPacket
# from . import structz
# import constants as const
