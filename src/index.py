import os
import sys

packages_path = os.path.join(os.path.split(__file__)[0], "packages")
sys.path.append(packages_path)

import itertools
import praw
import re
import socket
from openttd.client import Client, M_TCP
from openttd.date import OpenTTDDate
import json
import botocore 
import botocore.session 
from aws_secretsmanager_caching import SecretCache, SecretCacheConfig 

class Runner:
    def __init__(self):
        client = botocore.session.get_session().create_client('secretsmanager')
        cache_config = SecretCacheConfig()
        cache = SecretCache( config = cache_config, client = client)

        secret = cache.get_secret_string('reddit-credentials')
        creds  = json.loads(secret)
        self.reddituser = {
            'username': creds["bot_user"],
            'password': creds["bot_password"]
        }
        self.redditapp = {
            'app': creds["client_id"],
            'secret': creds["client_secret"]
        }

        # validate that all entries have been given in the laziest way possible
        for k, v in itertools.chain(self.reddituser.items(), self.redditapp.items()):
            assert v is not None, "A required configuration value is missing. Please check your environment variables."

    def processoffline(self, desc):
        """
        Parse the given text field for servers.
        :param desc: Subreddit description
        :return: string: New subreddit description
        """
        expr = re.compile('(🟢|🔴) \*.*:[0-9]* - .*')
        return expr.sub(self.processserver, desc)

    def processserver(self, m):
        """
        Process the given server string.
        :param m: Server string
        :return: string: New server string
        """
        print('Processing: ' + m.group(), flush=True)
        ip = re.search('(?:🟢|🔴) \*(.*):([0-9]*)', m.group())
        if ip:
            if not self.checkstatus(ip.group(1), ip.group(2)):
                # we're not up
                print('{host}:{port} is offline'.format(host=ip.group(1), port=ip.group(2)), flush=True)
                return '🔴 *{host}:{port} - offline*'.format(host=ip.group(1), port=ip.group(2))
            else:
                # we are up, let's try and pull some data
                port = int(ip.group(2))
                client = Client(ip.group(1), port)

                try:
                    client.connect(M_TCP)
                    gameinfo = client.getGameInfo()
                except socket.timeout:
                    print('{host}:{port} is online, but gameinfo call timed out'.format(
                        host=ip.group(1),
                        port=ip.group(2)
                    )
                    , flush=True)
                    return '🟢 *{host}:{port} - online*'.format(host=ip.group(1), port=ip.group(2))
                finally:
                    client.disconnect()

                if gameinfo is None:
                    # game info isn't available for some reason, but we are up
                    print('{host}:{port} is online, but unable to get gameinfo'.format(
                        host=ip.group(1),
                        port=ip.group(2)
                        , flush=True)
                    )
                    return '🟢 *{host}:{port} - online*'.format(host=ip.group(1), port=ip.group(2))

                currentdate = OpenTTDDate(gameinfo.game_date)
                # revision = gameInfo.server_revision
                # we're up and have data, let's return a formatted string
                print('{host}:{port} is online'.format(host=ip.group(1), port=ip.group(2)), flush=True)
                return '🟢 *{host}:{port} - {clients_on}/{clients_max} ({current_year})*'.format(
                    host=ip.group(1),
                    port=ip.group(2),
                    clients_on=gameinfo.clients_on,
                    clients_max=gameinfo.clients_max,
                    current_year=currentdate.toYMD()[0]
                )

        # this doesn't appear to be a properly formatted address, return what we got
        return m.group()

    @staticmethod
    def getStatusWidget(widgets):
        idx = 0
        while(not widgets[idx] is None):
            if "Server" in widgets[idx].shortName:
                return widgets[idx]

    @staticmethod
    def checkstatus(ip, port):
        """
        Check whether the given ip and port are connectable.
        Note: This is a socket check only, it doesn't check whether OpenTTD is running at the given address!
        :param ip: IP or hostname
        :param port: Port number
        :return: boolean: is the server up?
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(5)
        try:
            s.connect((ip, int(port)))
            s.shutdown(2)
            return True
        except (socket.error, socket.herror, socket.gaierror, socket.timeout):
            # timeouts, other errors, that kind of thing
            return False

    def run(self):
        """
        Execute the runner routine against the given runner instance.
        :param runner_instance: An instance of Runner
        :return: none
        """
        
        print("attempting to update...")
        r = praw.Reddit(user_agent='linux:r_openttd_status_script:3.0 (/u/efess, /u/luaduck)',
                        client_id=self.redditapp['app'],
                        client_secret=self.redditapp['secret'],
                        username=self.reddituser['username'],
                        password=self.reddituser['password'])
        subreddit = r.subreddits.search_by_name('openttd')[0]
        settings = subreddit.mod.settings()

        subreddit.mod.update(description=self.processoffline(settings['description']))

        # update widget
        widget = self.getStatusWidget(subreddit.widgets.sidebar)
        if not widget is None:
            widget.mod.update(text=self.processoffline(widget.text))

        print("Sidebar update successful")

def handler(event, context): 
    try:
        runner = Runner()
        runner.run()
    except:
        print("Unexpected error: "+sys.exc_info()[0])
        raise


if __name__ == '__main__':
    lambda_handler()