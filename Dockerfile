FROM python:3.9-alpine3.15

LABEL author="duck <me@duck.me.uk>"
LABEL maintainer="efess <efessel@gmail.com>"

RUN apk add --update git bash
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install --no-cache -r requirements.txt

COPY . /app

CMD ["python", "/app/update.py"]
